from flask import Flask, render_template, request, session, redirect, url_for
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, Email, EqualTo
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SECRET_KEY' ] ="AAEFRT765GFD45GCFG"

db = SQLAlchemy(app)

class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key =True)
    name = db.Column(db.String(64), unique=True)
    users = db.relationship( 'User', backref='role', lazy=True)

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key =True)
    username = db.Column(db.String(64), unique=True, index=True)
    password=db.column(db.String(64))
    role_id = db.Column(db.Integer, db.ForeignKey( 'roles.id' ))

class RegistrationForm(FlaskForm):
    username = StringField('username', validators=[DataRequired(), Length(min=2,max=20)])
    email = StringField('email', validators=[DataRequired(),Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
    validators = [DataRequired(),EqualTo('password')])
    submit = SubmitField('Sign Up')


@app.route('/')
def index():
    admin_role = Role(name='Admin')
    user = User(username='said', role=admin_role)
    db.session.add([user,admin_role])
    db.session.commit()

    Query = User.query.all()
    return render_template('index.html', data=Query)

@app.route('/register')
def register():
    myform = RegistrationForm()
    if myform.validate_on_submit():
        session['name'] = myform.name.data
        return redirect(url_for('index'))
    return render_template('register.html', form=myform)


app.run(debug=True)